require 'factory_girl'

FactoryGirl.define do
  factory :user do |u|
    u.name 'Test User'
    u.email 'user@test.com'
    u.password 'please'
  end
end

